import cn from 'classnames';
import styles from './App.module.css';

function App() {
  return (
    <div className={cn(styles.container, styles.testPostCss)}>
      <h1>Home</h1>
      <p>test PostCss</p>
      <span className={styles.testPostCssNested}>green</span>
    </div>
  );
}

export default App;
