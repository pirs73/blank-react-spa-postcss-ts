import React from 'react';
import { createRoot } from 'react-dom/client';
import reportWebVitals from './reportWebVitals';
import './index.css';
import App from './App';

const container = document.getElementById('root');
const root = container && createRoot(container);

root &&
  root.render(
    <React.StrictMode>
      <App />
    </React.StrictMode>,
  );

reportWebVitals();
