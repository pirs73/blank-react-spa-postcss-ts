import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders test PostCss link', () => {
  render(<App />);
  const linkElement = screen.getByText(/test PostCss/i);
  expect(linkElement).toBeInTheDocument();
});
